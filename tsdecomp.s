
.define TSNEW 1

decompsrc = tsget

decompress = tsdecrunch

.feature c_comments, leading_dot_in_identifiers

.define .label

/*

decrunch.asm

NMOS 6502 decompressor for data stored in TSCrunch format.

Copyright Antonio Savona 2022.

*/

.define FASTDECRUNCHER 1 ; 1.5% faster, at the cost of 8 bytes of code
.define INPLACE 1 ; Enables inplace decrunching. Use -i switch when crunching. 
.define RLEONLY 0

.label tsget 	= DECOMPVARS + 0	; 2 bytes
.label tstemp	= DECOMPVARS + 2
.label tsput 	= DECOMPVARS + 3	; 2 bytes
.label lzput 	= DECOMPVARS + 5	; 2 bytes


.if RLEONLY
.else
	.define USELZ 1
	.define USELZ2 1
.endif

.if INPLACE

.macro TS_DECRUNCH src
		lda #<src
		sta.zp tsget
		lda #>src
		sta.zp tsget + 1
		jsr tsdecrunch
.endmacro

.else

.macro TS_DECRUNCH(src,dst)
{
		lda #<src
		sta.zp tsget
		lda #>src
		sta.zp tsget + 1
		lda #<dst
		sta.zp tsput
		lda #>dst
		sta.zp tsput + 1
		jsr tsdecrunch
}

.endif

.if INPLACE
incpage:
			txa
			pha
			jsr getblock
			pla
			tax
			bne incedpage; jmp
.endif

tsdecrunch:
			jsr getblock
	decrunch:

	.if INPLACE
			ldy #$ff
		:	iny
			lda (tsget),y
			sta tsput , y	; last iteration trashes lzput, with no effect.
			cpy #2
			bne :- 
			
			pha
			tya
			ldy #0
			beq update_getonly
	.else
			ldy #0			
	.endif

	entry2:		
			POLLBLOCK

			lax (tsget),y
			
			bmi rleorlz
			beq done
	literal:
	.if USELZ2
			cmp #$40
			bcs lz2	
	.endif
	
	
	.if INPLACE

			inc tsget
			beq incpage
	incedpage:
		:	lda (tsget),y
			sta (tsput),y
			iny
			dex
			bne :-	
			tya
			tax
			; carry is clear
			ldy #0
	.else
			tay
		!:
			lda (tsget),y
			dey
			sta (tsput),y
			bne !-
			
			txa
			inx
	.endif
			
	updatezp_noclc:
			adc tsput
			sta tsput
			bcs updateput_hi
		putnoof:
			txa
		update_getonly:
			adc tsget
			sta tsget
			bcc entry2
			jsr getblock
			jmp entry2
			
	updateput_hi:
			inc tsput+1
			clc
			bcc putnoof
								
	rleorlz:
	.if USELZ
.if TSNEW
.else
			ldx #2
.endif
			alr #$7f
			bcs ts_delz		
	; RLE
	.else
			anc #$7f		//also clears carry
	.endif
			sta tstemp		; number of bytes to de-rle
			iny
			lda (tsget),y	; fetch rle byte
			ldy tstemp
	.if FASTDECRUNCHER

			dey
			sta (tsput),y
	.endif
	ts_derle_loop:
			dey
			sta (tsput),y
			bne ts_derle_loop

			; update zero page with a = runlen, x = 2 , y = 0 
			lda tstemp
.if TSNEW
			ldx #2
.endif
			; clc not needed as coming from anc
			bcc updatezp_noclc
			
	   done:
.if INPLACE	   
	   		pla
	   		sta (tsput),y
.endif	   		
			rts	
	; LZ2	
	.if USELZ2	
		lz2:
			eor #$ff - $40
			adc tsput
			sta lzput
			lda tsput + 1
			sbc #0
			sta lzput + 1 		
	
			; y already zero			
			lda (lzput),y
			sta (tsput),y
			iny		
			lda (lzput),y
			sta (tsput),y

			tya ; y = a = 1. 
			tax ; y = a = x = 1. a + carry = 2
			dey ; ldy #0
			beq updatezp_noclc
				
	 .endif
	 				
.if USELZ
	; LZ
	ts_delz:
			
			lsr 
			sta lzto + 1
			
			iny
			
			lda tsput

			bcc long
			sbc (tsget),y
			sta lzput
			lda tsput+1
	
			sbc #$00
		
			; lz MUST decrunch forward
	lz_entry:		
			sta lzput+1
.if TSNEW
			ldx #2
	lz_put:
			ldy #0
.else
			dey ; ldy #0
	lz_put:
.endif
	
	.if FASTDECRUNCHER
			lda (lzput),y
			sta (tsput),y
			iny
	.endif		
			lda (lzput),y
			sta (tsput),y
		:	iny
		
			lda (lzput),y
			sta (tsput),y
			
	lzto:	cpy #0
			bne :- 
			
			tya
			
			; update zero page with a = runlen, x = 2, y = 0
			ldy #0
                        ; clc not needed as we have len - 1 in A (from the encoder) and C = 1
			bcs updatezp_noclc

	long:			
.if TSNEW
			lda (tsget),y
			adc tsput
			sta lzput
			iny
			lax (tsget),y
			ora #$80
			adc tsput + 1
			sta lzput + 1		
			cpx #$80
			rol lzto + 1
			ldx #3
			bne lz_put
.else
			sec 
			sbc (tsget),y
			sta lzput
			lda tsput+1
			sbc #$01
			bcs lz_entry
.endif

getblock:
			php
			inc tsget + 1
			GETBLOCK tsget + 1
			plp
			rts

.endif	
